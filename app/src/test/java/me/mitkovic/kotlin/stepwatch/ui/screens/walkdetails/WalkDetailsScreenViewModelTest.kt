package me.mitkovic.kotlin.stepwatch.ui.screens.walkdetails

import app.cash.turbine.test
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.testing.data.repository.TestWalksRepositoryImpl
import me.mitkovic.kotlin.stepwatch.testing.ui.screens.walkdetails.TestWalkDetailsScreenViewModelProvider
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class WalkDetailsScreenViewModelTest {

    private lateinit var viewModel: WalkDetailsScreenViewModel
    private val testDispatcher = StandardTestDispatcher()

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        viewModel = TestWalkDetailsScreenViewModelProvider.provideWalkDetailsScreenViewModel(1)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `loadWalkDetails updates UI state correctly`() =
        runTest {
            val walk = Walk(1, "Walk 1", 1625132800000, 1625136400000, 1000.0, 1000, 5.1)
            val locations =
                listOf(
                    LocationData(
                        id = 1,
                        walkId = 1,
                        latitude = 59.9,
                        longitude = 10.7,
                        timestamp = 1625132800000,
                        speed = 5.0,
                        altitude = 100.0,
                    ),
                    LocationData(
                        id = 2,
                        walkId = 1,
                        latitude = 59.9,
                        longitude = 10.8,
                        timestamp = 1625136400000,
                        speed = 6.0,
                        altitude = 200.0,
                    ),
                )

            TestWalksRepositoryImpl.setWalkWithLocations(1, walk, locations)

            viewModel.walkUiState.test {
                // Initial state should not be loading
                val initialState = awaitItem()
                assertFalse(initialState.isLoading)

                // State after starting to fetch movie details
                val loadingState = awaitItem()
                assertTrue(loadingState.isLoading)

                // State after successfully fetching walk details
                val walkDetailsState = awaitItem()

                assertFalse(walkDetailsState.isLoading)
                assertEquals(walk, walkDetailsState.walk)
                assertEquals(locations, walkDetailsState.locations.locationsData)

                cancelAndConsumeRemainingEvents()
            }
        }
}
