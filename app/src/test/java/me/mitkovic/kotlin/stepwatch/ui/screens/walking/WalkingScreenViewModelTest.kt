package me.mitkovic.kotlin.stepwatch.ui.screens.walking

import android.app.Application
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import me.mitkovic.kotlin.stepwatch.logging.Logger
import me.mitkovic.kotlin.stepwatch.testing.common.TestLoggerImpl
import me.mitkovic.kotlin.stepwatch.testing.ui.screens.walking.TestWalkingScreenViewModelProvider
import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class WalkingScreenViewModelTest {

    private lateinit var viewModel: WalkingScreenViewModel
    private val testDispatcher = StandardTestDispatcher()

    @Mock
    lateinit var mockApplication: Application

    @Mock
    lateinit var mockLogger: Logger

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        MockitoAnnotations.openMocks(this)
        TestLoggerImpl.delegate = mockLogger
        viewModel = TestWalkingScreenViewModelProvider.provideWalkingScreenViewModel(mockApplication)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        TestLoggerImpl.delegate = null
    }

    /**
     * Test the logMessage function:
     * - The Logger should log an error with the provided message
     */
    @Test
    fun `logMessage logs error`() =
        runTest {
            // Given
            val errorMessage = "Test error message"

            // When
            viewModel.logMessage(errorMessage)

            // Then
            verify(mockLogger).logError(
                WalkingScreenViewModel::class.java.name,
                errorMessage,
                null,
            )
        }

    /**
     * Test the setIsTracking function:
     * - isTracking should be updated accordingly
     */
    @Test
    fun `setIsTracking updates isTracking correctly`() =
        runTest {
            // Initially not tracking
            assertFalse(viewModel.isTracking.value)

            // When: Set to true
            viewModel.setIsTracking(true)
            assertTrue(viewModel.isTracking.value)

            // When: Set to false
            viewModel.setIsTracking(false)
            assertFalse(viewModel.isTracking.value)
        }
}
