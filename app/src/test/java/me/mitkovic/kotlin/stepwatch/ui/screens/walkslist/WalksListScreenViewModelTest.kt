package me.mitkovic.kotlin.stepwatch.ui.screens.walkslist

import app.cash.turbine.test
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.testing.data.repository.TestWalksRepositoryImpl
import me.mitkovic.kotlin.stepwatch.testing.ui.screens.walkslist.TestWalksListScreenViewModelProvider
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class WalksListScreenViewModelTest {

    private lateinit var viewModel: WalksListScreenViewModel
    private val testDispatcher = StandardTestDispatcher()

    @OptIn(ExperimentalCoroutinesApi::class)
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        viewModel = TestWalksListScreenViewModelProvider.provideWalksListScreenViewModel()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `loadWalks updates UI state correctly`() =
        runTest {
            val fakeWalks =
                listOf(
                    Walk(1, "Walk 1", 1625132800000, 1625136400000, 1000.0, 1000, 5.1),
                    Walk(2, "Walk 2", 1625140000000, 1625143600000, 2000.0, 2000, 5.1),
                )

            // Preload the fake walks into the repository
            TestWalksRepositoryImpl.insertWalk(fakeWalks[0])
            TestWalksRepositoryImpl.insertWalk(fakeWalks[1])

            viewModel.walksUiState.test {
                // Expected: Initial state is not loading
                val initialState = awaitItem()
                assertFalse(initialState.isLoading)

                // Expected: Initial state is loading
                val loadingState = awaitItem()
                assertTrue(loadingState.isLoading)

                // Expected: Final state, not loading with updated data
                val finalState = awaitItem()

                assertFalse(finalState.isLoading)
                assertEquals(fakeWalks, finalState.walks.walks)

                // Ensure no more items are emitted
                cancelAndConsumeRemainingEvents()
            }
        }
}
