package me.mitkovic.kotlin.stepwatch.data.local

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk

@Dao
interface WalksDao {

    @Query("SELECT * FROM walks ORDER BY startTime DESC LIMIT 1")
    fun observeActiveWalk(): Flow<Walk?>

    @Transaction
    @Query("SELECT * FROM walks WHERE id = :walkId")
    fun getWalkWithLocations(walkId: Int): Flow<WalkWithLocations>

    @Query("SELECT * FROM walks")
    fun getAllWalks(): Flow<List<Walk>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWalk(walk: Walk): Long

    @Update
    suspend fun updateWalk(walk: Walk)

    @Delete
    suspend fun deleteWalk(walk: Walk)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLocationData(locationData: LocationData)
}
