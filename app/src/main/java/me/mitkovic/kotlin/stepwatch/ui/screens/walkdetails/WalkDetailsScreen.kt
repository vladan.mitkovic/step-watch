package me.mitkovic.kotlin.stepwatch.ui.screens.walkdetails

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.compose.LocalLifecycleOwner
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.PolylineOptions
import me.mitkovic.kotlin.stepwatch.R
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.logging.Logger
import me.mitkovic.kotlin.stepwatch.ui.MainAction
import me.mitkovic.kotlin.stepwatch.utils.DateTimeUtils
import me.mitkovic.kotlin.stepwatch.utils.DistanceUtils
import android.graphics.Color as AndroidColor

@Composable
fun WalkDetailsScreen(
    viewModel: WalkDetailsScreenViewModel = hiltViewModel(),
    paddingValues: PaddingValues,
    onAction: (MainAction) -> Unit,
) {
    val walkUiState by viewModel.walkUiState.collectAsStateWithLifecycle()

    LaunchedEffect(key1 = walkUiState) {
        if (walkUiState.walk != null) {
            val walk = walkUiState.walk
            if (walk != null) {
                onAction(MainAction.TitleTextChanged(walk.name))
            }
        }
    }

    Column(
        modifier =
            Modifier
                .padding(paddingValues)
                .fillMaxSize(),
    ) {
        if (walkUiState.locations.locationsData.isNotEmpty()) {
            WalkInfoBlock(walk = walkUiState.walk)
            Box(
                modifier =
                    Modifier
                        .weight(1f), // Take 50% of the height
            ) {
                WalkingMap(locations = walkUiState.locations.locationsData, logger = viewModel.logger)
            }
            Box(
                modifier =
                    Modifier
                        .weight(1f) // Take 50% of the height
                        .padding(dimensionResource(id = R.dimen.spacing_medium))
                        .clip(RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)))
                        .background(color = MaterialTheme.colorScheme.surface)
                        .border(
                            width = 1.dp,
                            color = MaterialTheme.colorScheme.outline,
                            shape = RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)),
                        ),
            ) {
                AltitudeGraph(locations = walkUiState.locations.locationsData)
            }
        } else {
            Text(
                text = stringResource(id = R.string.no_walk_distance_recorded),
                color = MaterialTheme.colorScheme.onSurface,
                modifier =
                    Modifier
                        .fillMaxWidth()
                        .padding(dimensionResource(id = R.dimen.spacing_medium)),
            )
        }
    }
}

@Composable
fun WalkInfoBlock(walk: Walk?) {
    walk?.let {
        Box(
            modifier =
                Modifier
                    .padding(dimensionResource(id = R.dimen.spacing_medium))
                    .clip(RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)))
                    .background(color = MaterialTheme.colorScheme.onBackground)
                    .border(
                        width = 1.dp,
                        color = MaterialTheme.colorScheme.outline,
                        shape = RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)),
                    ),
        ) {
            Column(
                modifier =
                    Modifier
                        .fillMaxWidth()
                        .padding(dimensionResource(id = R.dimen.spacing_medium)),
            ) {
                Box(
                    modifier =
                        Modifier
                            .fillMaxWidth()
                            .padding(bottom = dimensionResource(id = R.dimen.spacing_medium)),
                    contentAlignment = Alignment.CenterEnd,
                ) {
                    Text(
                        text = DateTimeUtils.formatDate(walk.stopTime),
                        color = MaterialTheme.colorScheme.onSurface,
                        style = MaterialTheme.typography.titleMedium,
                    )
                }
                WalkInfoRow(title = stringResource(id = R.string.start_time), value = DateTimeUtils.formatTime(walk.startTime))
                WalkInfoRow(title = stringResource(id = R.string.end_time), value = DateTimeUtils.formatTime(walk.stopTime))
                WalkInfoRow(
                    title = stringResource(id = R.string.total_time),
                    value = DateTimeUtils.formatElapsedTime(walk.startTime, walk.stopTime),
                )
                WalkInfoRow(title = stringResource(id = R.string.distance), value = "${DistanceUtils.metersToKilometers(walk.distance)} km")
            }
        }
    }
}

@Composable
fun WalkInfoRow(
    title: String,
    value: String,
) {
    Row(
        modifier =
            Modifier
                .fillMaxWidth()
                .padding(vertical = 4.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
    ) {
        Text(
            text = title,
            style = MaterialTheme.typography.titleMedium.copy(fontWeight = FontWeight.Bold),
            color = MaterialTheme.colorScheme.onSurface,
        )
        Text(
            text = value,
            style = MaterialTheme.typography.bodyMedium,
            color = MaterialTheme.colorScheme.onSurface,
        )
    }
}

@Composable
fun WalkingMap(
    locations: List<LocationData>,
    logger: Logger,
) {
    val mapView = rememberMapViewWithLifecycle()
    val context = LocalContext.current
    val isDarkTheme = isSystemInDarkTheme()

    Box(
        modifier =
            Modifier
                .padding(dimensionResource(id = R.dimen.spacing_medium))
                .clip(RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)))
                .background(color = MaterialTheme.colorScheme.surface)
                .border(
                    width = 1.dp,
                    color = MaterialTheme.colorScheme.outline,
                    shape = RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)),
                ),
    ) {
        AndroidView({ mapView }) { view ->
            view.getMapAsync { googleMap ->
                val path = locations.map { LatLng(it.latitude, it.longitude) }
                val polylineOptions =
                    PolylineOptions()
                        .addAll(path)
                        .color(if (isDarkTheme) AndroidColor.LTGRAY else AndroidColor.GRAY)

                googleMap.addPolyline(polylineOptions)

                if (path.isNotEmpty()) {
                    val boundsBuilder = LatLngBounds.Builder()
                    path.forEach { boundsBuilder.include(it) }
                    val bounds = boundsBuilder.build()
                    val padding = 100
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding))
                }

                if (isDarkTheme) {
                    try {
                        val success =
                            googleMap.setMapStyle(
                                MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style_dark),
                            )
                        if (!success) {
                            logger.logDebug(WalkDetailsScreenViewModel::class.java.name, "Failed to set map style")
                        }
                        logger.logDebug(WalkDetailsScreenViewModel::class.java.name, "Succeed to set map style")
                    } catch (e: Exception) {
                        logger.logDebug(WalkDetailsScreenViewModel::class.java.name, "Error setting map style: $e")
                    }
                }
            }
        }
    }
}

@Composable
fun rememberMapViewWithLifecycle(): MapView {
    val context = LocalContext.current
    val mapView =
        remember {
            MapView(context).apply {
                id = R.id.map
            }
        }

    val lifecycleObserver = rememberMapLifecycleObserver(mapView)
    val lifecycle = LocalLifecycleOwner.current.lifecycle
    DisposableEffect(lifecycle) {
        lifecycle.addObserver(lifecycleObserver)
        onDispose {
            lifecycle.removeObserver(lifecycleObserver)
        }
    }

    return mapView
}

@Composable
fun rememberMapLifecycleObserver(mapView: MapView): LifecycleEventObserver =
    remember {
        LifecycleEventObserver { _, event ->
            when (event) {
                Lifecycle.Event.ON_CREATE -> mapView.onCreate(null)
                Lifecycle.Event.ON_START -> mapView.onStart()
                Lifecycle.Event.ON_RESUME -> mapView.onResume()
                Lifecycle.Event.ON_PAUSE -> mapView.onPause()
                Lifecycle.Event.ON_STOP -> mapView.onStop()
                Lifecycle.Event.ON_DESTROY -> mapView.onDestroy()
                else -> throw IllegalStateException()
            }
        }
    }

@Composable
fun AltitudeGraph(locations: List<LocationData>) {
    val altitudes = locations.map { it.altitude }
    val isDarkTheme = isSystemInDarkTheme()

    Column(
        modifier =
            Modifier
                .fillMaxSize()
                .padding(dimensionResource(id = R.dimen.spacing_medium)),
    ) {
        Text(
            stringResource(
                id = R.string.altitude_graph,
            ),
            color = MaterialTheme.colorScheme.onSurface,
            style = MaterialTheme.typography.titleMedium,
        )
        Canvas(modifier = Modifier.fillMaxSize()) {
            val path = Path()
            val maxAltitude = altitudes.maxOrNull()?.toFloat() ?: 1.0f
            val minAltitude = altitudes.minOrNull()?.toFloat() ?: 0.0f
            val altitudeRange = maxAltitude - minAltitude

            altitudes.forEachIndexed { index, altitude ->
                val x = (index.toFloat() / altitudes.size) * size.width
                val normalizedAltitude = (altitude.toFloat() - minAltitude) / altitudeRange
                val y = size.height / 2 - normalizedAltitude * (size.height / 2)
                if (index == 0) {
                    path.moveTo(x, y)
                } else {
                    path.lineTo(x, y)
                }
            }
            drawPath(path, color = if (isDarkTheme) Color.LightGray else Color.DarkGray, style = Stroke(width = 2.dp.toPx()))
        }
    }
}
