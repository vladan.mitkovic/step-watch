package me.mitkovic.kotlin.stepwatch.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import me.mitkovic.kotlin.stepwatch.data.local.WalkWithLocations
import me.mitkovic.kotlin.stepwatch.data.local.WalksDao
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import javax.inject.Inject

class WalksRepositoryImpl
    @Inject
    constructor(
        private val walksDao: WalksDao,
    ) : WalksRepository {

        override fun getActiveWalk(): Flow<Walk?> = walksDao.observeActiveWalk()

        override fun getWalks(): Flow<List<Walk>> = walksDao.getAllWalks()

        override fun getWalkWithLocations(walkId: Int): Flow<WalkWithLocations?> =
            walksDao
                .getWalkWithLocations(walkId)
                .map { it }

        override suspend fun insertWalk(walk: Walk): Long = walksDao.insertWalk(walk)

        override suspend fun updateWalk(walk: Walk) = walksDao.updateWalk(walk)

        override suspend fun deleteWalk(walk: Walk) {
            walksDao.deleteWalk(walk)
        }

        override suspend fun insertLocationData(locationData: LocationData) = walksDao.insertLocationData(locationData)
    }
