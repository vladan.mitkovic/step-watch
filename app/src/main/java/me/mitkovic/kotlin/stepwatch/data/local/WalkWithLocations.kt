package me.mitkovic.kotlin.stepwatch.data.local

import androidx.room.Embedded
import androidx.room.Relation
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk

data class WalkWithLocations(
    @Embedded val walk: Walk,
    @Relation(
        parentColumn = "id",
        entityColumn = "walkId",
    )
    val locations: List<LocationData>,
)
