package me.mitkovic.kotlin.stepwatch.ui.screens.walkdetails

import androidx.compose.runtime.Immutable
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.stateIn
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.data.repository.WalksRepository
import me.mitkovic.kotlin.stepwatch.logging.Logger
import javax.inject.Inject

data class WalkingState(
    val isLoading: Boolean = false,
    val walk: Walk? = null,
    val locations: LocationsData = LocationsData(emptyList()),
    val error: String? = null,
)

@Immutable
data class LocationsData(
    val locationsData: List<LocationData>,
)

@HiltViewModel
class WalkDetailsScreenViewModel
    @Inject
    constructor(
        walksRepository: WalksRepository,
        val logger: Logger,
        savedStateHandle: SavedStateHandle,
    ) : ViewModel() {

        private val id: Int = checkNotNull(savedStateHandle["id"]) { "Walk ID is missing" }

        val walkUiState =
            walksRepository
                .getWalkWithLocations(id)
                .map { walkWithLocations ->
                    WalkingState(
                        isLoading = false,
                        walk = walkWithLocations?.walk,
                        locations = LocationsData(walkWithLocations?.locations ?: emptyList()),
                    )
                }.onStart { emit(WalkingState(isLoading = true)) }
                .catch { e ->
                    logger.logDebug("WalkDetailsScreen", e.message.toString())
                    emit(WalkingState(error = e.message))
                }.stateIn(
                    scope = viewModelScope,
                    started = SharingStarted.WhileSubscribed(5_000),
                    initialValue = WalkingState(),
                )
    }
