package me.mitkovic.kotlin.stepwatch.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk

@Database(entities = [Walk::class, LocationData::class], version = 1, exportSchema = false)
abstract class WalksDatabase : RoomDatabase() {

    abstract fun getWalksDao(): WalksDao
}
