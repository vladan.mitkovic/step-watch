package me.mitkovic.kotlin.stepwatch.ui.screens.walking

import android.app.Application
import android.content.Intent
import android.os.Build
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.data.repository.WalksRepository
import me.mitkovic.kotlin.stepwatch.logging.Logger
import me.mitkovic.kotlin.stepwatch.service.LocationService
import me.mitkovic.kotlin.stepwatch.service.LocationService.Companion.ACTION_START
import me.mitkovic.kotlin.stepwatch.service.LocationService.Companion.ACTION_STOP
import javax.inject.Inject

@HiltViewModel
class WalkingScreenViewModel
    @Inject
    constructor(
        private val app: Application,
        private val walksRepository: WalksRepository,
        private val logger: Logger,
    ) : AndroidViewModel(app) {

        private val _isTracking = MutableStateFlow(false)
        val isTracking: StateFlow<Boolean> get() = _isTracking

        val allWalks: StateFlow<List<Walk>?> =
            walksRepository
                .getWalks()
                .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

        val currentWalk: StateFlow<Walk?> =
            walksRepository
                .getActiveWalk()
                .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), null)

        fun startTracking() {
            logger.logDebug(WalkingScreenViewModel::class.java.name, "startTracking")

            val intent =
                Intent(app, LocationService::class.java).apply {
                    action = ACTION_START
                }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                app.startForegroundService(intent)
            } else {
                app.startService(intent)
            }
            _isTracking.value = true
        }

        fun stopTracking() {
            logger.logDebug(WalkingScreenViewModel::class.java.name, "stopTracking")

            val intent =
                Intent(app, LocationService::class.java).apply {
                    action = ACTION_STOP
                }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                app.startForegroundService(intent)
            } else {
                app.startService(intent)
            }
            _isTracking.value = false
        }

        fun setIsTracking(isTracking: Boolean) {
            _isTracking.value = isTracking
        }

        fun saveWalk(updatedWalk: Walk) {
            viewModelScope.launch {
                logger.logDebug(WalkingScreenViewModel::class.java.name, "saveWalk updatedWalk: $updatedWalk")
                walksRepository.updateWalk(updatedWalk)
            }
        }

        fun logMessage(message: String) {
            logger.logError(WalkingScreenViewModel::class.java.name, message, null)
        }
    }
