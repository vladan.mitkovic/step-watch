package me.mitkovic.kotlin.stepwatch.testing.data.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import me.mitkovic.kotlin.stepwatch.data.local.WalkWithLocations
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.data.repository.WalksRepository

object TestWalksRepositoryImpl : WalksRepository {
    private var storedWalks = mutableMapOf<Int, WalkWithLocations>()
    private val walks = mutableListOf<Walk>()
    private val locationData = mutableListOf<LocationData>()

    fun setWalkWithLocations(
        walkId: Int,
        walk: Walk,
        locations: List<LocationData>,
    ) {
        storedWalks[walkId] = WalkWithLocations(walk, locations)
    }

    override fun getActiveWalk(): Flow<Walk?> {
        // ("Not yet implemented")
        return flowOf(null)
    }

    override fun getWalks(): Flow<List<Walk>> = flowOf(walks)

    override fun getWalkWithLocations(walkId: Int): Flow<WalkWithLocations?> =
        flow {
            emit(storedWalks[walkId])
        }

    override suspend fun insertWalk(walk: Walk): Long {
        walks.add(walk)
        return walk.id.toLong()
    }

    override suspend fun updateWalk(walk: Walk) {
        val index = walks.indexOfFirst { it.id == walk.id }
        if (index != -1) {
            walks[index] = walk
        }
    }

    override suspend fun deleteWalk(walk: Walk) {
        walks.remove(walk)
    }

    override suspend fun insertLocationData(locationData: LocationData) {
        this.locationData.add(locationData)
    }
}
