package me.mitkovic.kotlin.stepwatch.testing.ui.screens.walking

import android.app.Application
import me.mitkovic.kotlin.stepwatch.testing.common.TestLoggerImpl
import me.mitkovic.kotlin.stepwatch.testing.data.repository.TestWalksRepositoryImpl
import me.mitkovic.kotlin.stepwatch.ui.screens.walking.WalkingScreenViewModel

object TestWalkingScreenViewModelProvider {

    fun provideWalkingScreenViewModel(application: Application): WalkingScreenViewModel =
        WalkingScreenViewModel(
            app = application,
            walksRepository = TestWalksRepositoryImpl,
            logger = TestLoggerImpl,
        )
}
