package me.mitkovic.kotlin.stepwatch.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

object DateTimeUtils {
    private const val DATE_TIME_FORMAT = "MMM d ''yy HH:mm:ss"

    fun formatDateTime(dateTime: Long): String {
        val sdf = SimpleDateFormat(DATE_TIME_FORMAT, Locale.getDefault())
        return sdf.format(Date(dateTime))
    }

    fun formatDate(timeInMillis: Long): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return dateFormat.format(Date(timeInMillis))
    }

    fun formatTime(timeInMillis: Long): String {
        val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        return dateFormat.format(Date(timeInMillis))
    }

    fun formatElapsedTime(
        startTime: Long,
        stopTime: Long,
    ): String {
        val elapsedMillis = stopTime - startTime
        val hours = (elapsedMillis / 3600000).toInt()
        val minutes = ((elapsedMillis % 3600000) / 60000).toInt()
        val seconds = ((elapsedMillis % 60000) / 1000).toInt()
        return String.format(Locale.US, "%02d:%02d:%02d", hours, minutes, seconds)
    }
}
