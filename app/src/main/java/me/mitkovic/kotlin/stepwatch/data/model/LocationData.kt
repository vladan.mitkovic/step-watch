package me.mitkovic.kotlin.stepwatch.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "location_data",
    foreignKeys = [
        ForeignKey(
            entity = Walk::class,
            parentColumns = ["id"],
            childColumns = ["walkId"],
            onDelete = ForeignKey.CASCADE,
        ),
    ],
    indices = [Index(value = ["walkId"])],
)
data class LocationData(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val walkId: Int,
    val latitude: Double,
    val longitude: Double,
    val timestamp: Long,
    val speed: Double,
    val altitude: Double,
)
