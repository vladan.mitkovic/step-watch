package me.mitkovic.kotlin.stepwatch.ui.screens.walking

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import kotlinx.coroutines.delay
import me.mitkovic.kotlin.stepwatch.R
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.ui.MainAction
import me.mitkovic.kotlin.stepwatch.ui.commons.StyledTextButton
import me.mitkovic.kotlin.stepwatch.utils.DateTimeUtils
import me.mitkovic.kotlin.stepwatch.utils.DistanceUtils
import java.util.Locale

@Composable
fun WalkingScreen(
    viewModel: WalkingScreenViewModel = hiltViewModel(),
    paddingValues: PaddingValues,
    launchedFromNotification: () -> Boolean,
    onAction: (MainAction) -> Unit,
    onWalkSaved: (Int) -> Unit,
) {
    val currentWalk by viewModel.currentWalk.collectAsStateWithLifecycle()
    val isTrackingCurrentWalk by viewModel.isTracking.collectAsStateWithLifecycle()
    val allWalks by viewModel.allWalks.collectAsStateWithLifecycle() // for graph

    // putting the app into background and clicking on location foreground service notification
    // will continue displaying current walking screen
    LaunchedEffect(launchedFromNotification()) {
        if (launchedFromNotification()) {
            viewModel.setIsTracking(true)
            // put back launchedFromNotification value to false
            onAction(MainAction.LaunchedFromNotificationChanged(false))
        }
    }

    // Local state for displaying time on the screen
    var elapsedTime by remember { mutableStateOf("00:00:00") }
    var showDetails by remember { mutableStateOf(false) }

    LaunchedEffect(currentWalk) {
        while (currentWalk?.stopTime == 0L) {
            currentWalk?.let { walk ->
                val currentMillis = System.currentTimeMillis()
                elapsedTime = DateTimeUtils.formatElapsedTime(walk.startTime, currentMillis)
            }
            delay(1000L) // display updated time on the screen every second
        }
        elapsedTime = "00:00:00"
    }

    Box(
        modifier =
            Modifier
                .padding(paddingValues)
                .fillMaxSize(),
        contentAlignment = Alignment.TopCenter,
    ) {
        if (isTrackingCurrentWalk) {
            val distance = currentWalk?.distance ?: 0.0
            val speed = currentWalk?.speed ?: 0.0

            Column(
                modifier =
                    Modifier
                        .fillMaxWidth()
                        .padding(dimensionResource(id = R.dimen.spacing_medium)),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Text(
                    text = stringResource(id = R.string.time),
                    color = MaterialTheme.colorScheme.onSurface,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold,
                )
                Text(
                    text = elapsedTime,
                    color = MaterialTheme.colorScheme.onSurface,
                    fontSize = 48.sp,
                    fontWeight = FontWeight.Bold,
                )
                Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.spacing_xlarge)))
                Text(
                    text = stringResource(id = R.string.avg_speed),
                    color = MaterialTheme.colorScheme.onSurface,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold,
                )
                Text(
                    text = "${String.format(Locale.US, "%.1f", speed)} KM/H",
                    color = MaterialTheme.colorScheme.onSurface,
                    fontSize = 48.sp,
                    fontWeight = FontWeight.Bold,
                )
                Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.spacing_xlarge)))
                Text(
                    text = stringResource(id = R.string.walk_distance),
                    color = MaterialTheme.colorScheme.onSurface,
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold,
                )
                Text(
                    text = "${String.format(Locale.US, "%.2f", distance)} M",
                    color = MaterialTheme.colorScheme.onSurface,
                    fontSize = 48.sp,
                    fontWeight = FontWeight.Bold,
                )
            }
        } else {
            allWalks?.let {
                Column(
                    modifier =
                        Modifier
                            .fillMaxWidth()
                            .height(dimensionResource(id = R.dimen.walks_graph_height))
                            .padding(dimensionResource(id = R.dimen.spacing_medium))
                            .clip(RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)))
                            .background(color = MaterialTheme.colorScheme.surface)
                            .border(
                                width = 1.dp,
                                color = MaterialTheme.colorScheme.outline,
                                shape = RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)),
                            ),
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    WalkDistanceGraph(walks = allWalks)
                }
            }
        }

        Box(
            modifier =
                Modifier
                    .fillMaxSize(),
            contentAlignment = Alignment.BottomCenter,
        ) {
            StyledTextButton(
                text = if (isTrackingCurrentWalk) stringResource(id = R.string.stop) else stringResource(id = R.string.start),
                modifier = Modifier.padding(start = dimensionResource(id = R.dimen.spacing_medium)),
                onClick = {
                    if (isTrackingCurrentWalk) {
                        viewModel.stopTracking()
                        onAction(MainAction.LaunchedFromNotificationChanged(false))
                        showDetails = true
                    } else {
                        viewModel.startTracking()
                        onAction(MainAction.ShowActionsChanged(false))
                    }
                },
            )
        }
    }

    if (showDetails) {
        currentWalk?.let { walk ->
            var walkName by remember(currentWalk) {
                mutableStateOf(currentWalk?.name ?: "")
            }

            val totalTime = DateTimeUtils.formatElapsedTime(walk.startTime, walk.stopTime)
            val distanceInKm = DistanceUtils.metersToKilometers(walk.distance)

            WalkDetailsDialog(
                name = walkName,
                onNameChange = { walkName = it },
                totalTime = totalTime,
                distanceInKm = distanceInKm,
                onSave = {
                    val updatedWalk = walk.copy(name = walkName)
                    viewModel.saveWalk(updatedWalk)
                    onAction(MainAction.ShowActionsChanged(true))
                    onWalkSaved(walk.id)
                },
            )
        }
    }
}

@Composable
fun WalkDetailsDialog(
    name: String,
    onNameChange: (String) -> Unit,
    totalTime: String,
    distanceInKm: String,
    onSave: () -> Unit,
) {
    Box(
        modifier =
            Modifier
                .fillMaxSize()
                .background(MaterialTheme.colorScheme.background)
                .padding(dimensionResource(id = R.dimen.spacing_medium)),
    ) {
        Column(
            modifier =
                Modifier
                    .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Column(
                modifier =
                    Modifier
                        .fillMaxWidth()
                        .weight(1f),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
            ) {
                Text(
                    text = stringResource(id = R.string.walk_details),
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold,
                    color = MaterialTheme.colorScheme.onSurface,
                )
                Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.spacing_medium)))
                OutlinedTextField(
                    value = name,
                    onValueChange = onNameChange,
                    label = { Text(stringResource(id = R.string.name)) },
                    colors =
                        OutlinedTextFieldDefaults.colors(
                            focusedBorderColor = MaterialTheme.colorScheme.onPrimary,
                            focusedLabelColor = MaterialTheme.colorScheme.onPrimary,
                            cursorColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                    modifier = Modifier.fillMaxWidth(),
                )
                Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.spacing_medium)))
                Text(
                    text =
                        buildAnnotatedString {
                            withStyle(style = MaterialTheme.typography.titleMedium.toSpanStyle()) {
                                append(stringResource(id = R.string.total_time))
                            }
                            append(totalTime)
                        },
                    color = MaterialTheme.colorScheme.onSurface,
                )
                Text(
                    text =
                        buildAnnotatedString {
                            withStyle(style = MaterialTheme.typography.titleMedium.toSpanStyle()) {
                                append(stringResource(id = R.string.distance))
                            }
                            append("$distanceInKm km")
                        },
                    color = MaterialTheme.colorScheme.onSurface,
                )
            }

            Spacer(modifier = Modifier.height(dimensionResource(id = R.dimen.spacing_medium)))

            StyledTextButton(
                text = stringResource(id = R.string.save),
                modifier =
                    Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(start = dimensionResource(id = R.dimen.spacing_medium)),
                onClick = onSave,
            )
        }
    }
}

@Composable
fun WalkDistanceGraph(walks: List<Walk>?) {
    val distances = walks?.map { it.distance }
    val isDarkTheme = isSystemInDarkTheme()

    Column(
        modifier =
            Modifier
                .fillMaxWidth()
                .padding(dimensionResource(id = R.dimen.spacing_medium)),
    ) {
        Text(
            stringResource(id = R.string.walks_distance_graph),
            color = MaterialTheme.colorScheme.onSurface,
            style = MaterialTheme.typography.titleMedium,
        )
        Canvas(modifier = Modifier.fillMaxSize()) {
            val path = Path()
            val maxDistance = distances?.maxOrNull()?.toFloat() ?: 1.0f
            val minDistance = distances?.minOrNull()?.toFloat() ?: 0.0f
            val distanceRange = maxDistance - minDistance

            distances?.forEachIndexed { index, distance ->
                val x = (index.toFloat() / distances.size) * size.width
                val normalizedDistance = (distance.toFloat() - minDistance) / distanceRange
                val y = size.height / 2 - normalizedDistance * (size.height / 2)
                if (index == 0) {
                    path.moveTo(x, y)
                } else {
                    path.lineTo(x, y)
                }
            }
            drawPath(
                path,
                color = if (isDarkTheme) Color.LightGray else Color.DarkGray,
                style = Stroke(width = 2.dp.toPx()),
            )
        }
    }
}
