package me.mitkovic.kotlin.stepwatch.logging

import timber.log.Timber
import javax.inject.Inject

class TimberLogger
    @Inject
    constructor() : Logger {

        override fun logDebug(
            tag: String,
            message: String,
        ) {
            Timber.tag(tag).d(message)
        }

        override fun logError(
            tag: String,
            message: String?,
            throwable: Throwable?,
        ) {
            Timber.e(throwable, message)
        }
    }
