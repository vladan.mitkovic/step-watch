package me.mitkovic.kotlin.stepwatch.ui.commons

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.font.FontWeight
import me.mitkovic.kotlin.stepwatch.R

@Composable
fun StyledIconButton(
    imageVector: ImageVector,
    contentDescription: String,
    modifier: Modifier = Modifier,
    alignment: Alignment = Alignment.Center, // Default alignment is Center
    onClick: () -> Unit,
) {
    Box(modifier = modifier) {
        // The Box here uses the passed modifier, including any alignment
        Surface(
            modifier =
                Modifier
                    .clip(CircleShape) // Apply the rounded corner shape
                    .clickable(onClick = onClick) // Click action
                    .align(alignment),
            // Apply alignment within Box
            color = MaterialTheme.colorScheme.onPrimary, // Button background color
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier =
                    Modifier
                        .size(dimensionResource(id = R.dimen.spacing_large_button), dimensionResource(id = R.dimen.spacing_large_button)),
            ) {
                Icon(
                    imageVector = imageVector,
                    contentDescription = contentDescription,
                    tint = MaterialTheme.colorScheme.primary, // Icon color, adapting to theme
                    modifier = Modifier.size(dimensionResource(id = R.dimen.spacing_large)), // Icon size
                )
            }
        }
    }
}

@Composable
fun StyledTextButton(
    text: String,
    modifier: Modifier = Modifier,
    alignment: Alignment = Alignment.Center,
    onClick: () -> Unit,
) {
    Box(modifier = modifier) {
        Surface(
            modifier =
                Modifier
                    .clip(CircleShape)
                    .clickable(onClick = onClick)
                    .align(alignment),
            color = MaterialTheme.colorScheme.onPrimary,
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier =
                    Modifier
                        .size(dimensionResource(id = R.dimen.spacing_xxxlarge), dimensionResource(id = R.dimen.spacing_xxxlarge)),
            ) {
                Text(
                    text = text,
                    color = MaterialTheme.colorScheme.primary,
                    fontWeight = FontWeight.Bold,
                )
            }
        }
    }
}
