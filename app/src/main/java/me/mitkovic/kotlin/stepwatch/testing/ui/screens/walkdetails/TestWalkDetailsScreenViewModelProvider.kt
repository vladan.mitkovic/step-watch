package me.mitkovic.kotlin.stepwatch.testing.ui.screens.walkdetails

import androidx.lifecycle.SavedStateHandle
import me.mitkovic.kotlin.stepwatch.testing.common.TestLoggerImpl
import me.mitkovic.kotlin.stepwatch.testing.data.repository.TestWalksRepositoryImpl
import me.mitkovic.kotlin.stepwatch.ui.screens.walkdetails.WalkDetailsScreenViewModel

object TestWalkDetailsScreenViewModelProvider {

    fun provideWalkDetailsScreenViewModel(id: Int): WalkDetailsScreenViewModel {
        val savedStateHandle = SavedStateHandle(mapOf("id" to id))
        return WalkDetailsScreenViewModel(
            walksRepository = TestWalksRepositoryImpl,
            logger = TestLoggerImpl,
            savedStateHandle = savedStateHandle,
        )
    }
}
