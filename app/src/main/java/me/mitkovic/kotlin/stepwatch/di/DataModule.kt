package me.mitkovic.kotlin.stepwatch.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import me.mitkovic.kotlin.stepwatch.data.local.WalksDao
import me.mitkovic.kotlin.stepwatch.data.local.WalksDatabase
import me.mitkovic.kotlin.stepwatch.data.repository.WalksRepository
import me.mitkovic.kotlin.stepwatch.data.repository.WalksRepositoryImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideWalksDatabase(
        @ApplicationContext context: Context,
    ): WalksDatabase =
        Room
            .databaseBuilder(context.applicationContext, WalksDatabase::class.java, "walks_db")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideWalksDao(database: WalksDatabase): WalksDao = database.getWalksDao()

    @Provides
    @Singleton
    fun provideWalkRepository(walkDao: WalksDao): WalksRepository = WalksRepositoryImpl(walkDao)
}
