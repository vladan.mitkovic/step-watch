package me.mitkovic.kotlin.stepwatch.ui.navigation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import me.mitkovic.kotlin.stepwatch.R
import me.mitkovic.kotlin.stepwatch.ui.MainAction
import me.mitkovic.kotlin.stepwatch.ui.screens.walkdetails.WalkDetailsScreen
import me.mitkovic.kotlin.stepwatch.ui.screens.walkdetails.WalkDetailsScreenViewModel
import me.mitkovic.kotlin.stepwatch.ui.screens.walking.WalkingScreen
import me.mitkovic.kotlin.stepwatch.ui.screens.walking.WalkingScreenViewModel
import me.mitkovic.kotlin.stepwatch.ui.screens.walkslist.WalksListScreen
import me.mitkovic.kotlin.stepwatch.ui.screens.walkslist.WalksListScreenViewModel

@Composable
fun StepWatchAppNavigation(
    navHostController: NavHostController,
    paddingValues: PaddingValues,
    launchedFromNotification: () -> Boolean,
    onAction: (MainAction) -> Unit,
) {
    NavHost(
        navController = navHostController,
        startDestination = Destination.Home,
    ) {
        composable<Destination.Home> {
            onAction(MainAction.TitleTextChanged(stringResource(R.string.app_name)))
            onAction(MainAction.ShowActionsChanged(true))
            onAction(MainAction.ShowBackIconChanged(false))

            val walkingScreenViewModel: WalkingScreenViewModel = hiltViewModel()
            WalkingScreen(
                viewModel = walkingScreenViewModel,
                paddingValues = paddingValues,
                launchedFromNotification = { launchedFromNotification() },
                onAction = onAction,
                onWalkSaved = { id ->
                    navHostController.navigate(Destination.WalkDetails(id))
                },
            )
        }

        composable<Destination.Walks> {
            onAction(MainAction.TitleTextChanged(stringResource(R.string.walks)))
            onAction(MainAction.ShowActionsChanged(false))
            onAction(MainAction.ShowBackIconChanged(true))

            val walksListScreenViewModel: WalksListScreenViewModel = hiltViewModel()
            WalksListScreen(
                viewModel = walksListScreenViewModel,
                paddingValues = paddingValues,
                onWalkClick = { id ->
                    navHostController.navigate(Destination.WalkDetails(id))
                },
            )
        }

        composable<Destination.WalkDetails> { backStackEntry ->

            onAction(MainAction.ShowActionsChanged(false))
            onAction(MainAction.ShowBackIconChanged(true))

            val movieDetailsViewModel: WalkDetailsScreenViewModel = hiltViewModel()

            WalkDetailsScreen(
                viewModel = movieDetailsViewModel,
                paddingValues = paddingValues,
                onAction = onAction,
            )
        }
    }
}
