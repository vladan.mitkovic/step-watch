package me.mitkovic.kotlin.stepwatch.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFF295459)
val SurfaceDark = Color(0xFF121212)
val OnPrimaryDark = Color(0xFFb2dfd3)
val OnBackgroundDark = Color(0xFF28282b)
val OutlineDark = Color.Gray

val Purple40 = Color(0xFFFAFAFA)
val SurfaceLight = Color(0xFFFFFFFF)
val OnPrimaryLight = Color(0xFF307071)
val OnBackgroundLight = Color(0xFFFFFFFF)
val OutlineLight = Color.LightGray
