package me.mitkovic.kotlin.stepwatch.ui.navigation

import kotlinx.serialization.Serializable

sealed class Destination {
    @Serializable
    object Home : Destination()

    @Serializable
    object Walks : Destination()

    @Serializable
    data class WalkDetails(
        val id: Int,
    ) : Destination()
}
