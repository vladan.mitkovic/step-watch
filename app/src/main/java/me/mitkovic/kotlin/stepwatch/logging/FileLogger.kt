package me.mitkovic.kotlin.stepwatch.logging

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import timber.log.Timber
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FileLogger
    @Inject
    constructor(
        @ApplicationContext private val context: Context,
    ) : Logger {

        private val logFile: File by lazy {
            val dateFormat = SimpleDateFormat("yyyy_MM_dd", Locale.getDefault())
            val currentDate = dateFormat.format(Date())
            File(context.filesDir, "app_logs_$currentDate.txt")
        }

        private val fileWriter: FileWriter by lazy {
            FileWriter(logFile, true)
        }

        private val dateTimeFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS Z", Locale.getDefault())

        override fun logDebug(
            tag: String,
            message: String,
        ) {
            try {
                val timestamp = dateTimeFormat.format(Date())
                fileWriter.append("$timestamp $tag: $message\n")
                fileWriter.flush()
                Timber.tag(tag).d("$timestamp $message\n")
                deleteOldLogs() // Check and delete old logs if necessary
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        override fun logError(
            tag: String,
            message: String?,
            throwable: Throwable?,
        ) {
            // ("Not yet implemented")
        }

        private fun deleteOldLogs() {
            val dateFormat = SimpleDateFormat("yyyy_MM_dd", Locale.getDefault())
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, -7)
            val cutoffDate = dateFormat.format(calendar.time)

            val files = context.filesDir.listFiles()
            files?.forEach { file ->
                if (file.name.startsWith("app_logs_") && file.name < "app_logs_$cutoffDate.txt") {
                    file.delete()
                }
            }
        }
    }
