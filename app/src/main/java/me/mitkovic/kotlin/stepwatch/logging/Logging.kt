package me.mitkovic.kotlin.stepwatch.logging

interface Logger {

    fun logDebug(
        tag: String,
        message: String,
    )

    fun logError(
        tag: String,
        message: String?,
        throwable: Throwable?,
    )
}
