package me.mitkovic.kotlin.stepwatch.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.mitkovic.kotlin.stepwatch.logging.Logger
import me.mitkovic.kotlin.stepwatch.logging.TimberLogger
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object LoggerModule {

    /*
    @Provides
    @Singleton
    fun providesLogger(
        @ApplicationContext appContext: Context,
    ): Logger = FileLogger(appContext)
     */

    @Provides
    @Singleton
    fun providesLogger(): Logger = TimberLogger()
}
