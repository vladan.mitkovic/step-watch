package me.mitkovic.kotlin.stepwatch.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "walks")
data class Walk(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val name: String,
    val startTime: Long,
    val stopTime: Long,
    val distance: Double,
    val steps: Int,
    val speed: Double,
)
