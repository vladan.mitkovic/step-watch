package me.mitkovic.kotlin.stepwatch.ui.screens.walkslist

import androidx.compose.runtime.Immutable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.data.repository.WalksRepository
import javax.inject.Inject

data class WalksUiState(
    val isLoading: Boolean = false,
    val walks: Walks = Walks(emptyList()),
    val error: String? = null,
)

@Immutable
data class Walks(
    val walks: List<Walk>,
)

@HiltViewModel
class WalksListScreenViewModel
    @Inject
    constructor(
        private val walksRepository: WalksRepository,
    ) : ViewModel() {

        val walksUiState =
            walksRepository
                .getWalks()
                .map { walks ->
                    WalksUiState(
                        isLoading = false,
                        walks = Walks(walks),
                    )
                }.onStart { emit(WalksUiState(isLoading = true)) }
                .catch { e ->
                    emit(WalksUiState(error = e.message))
                }.stateIn(
                    scope = viewModelScope,
                    started = SharingStarted.WhileSubscribed(5_000),
                    initialValue = WalksUiState(),
                )

        fun removeWalk(walk: Walk) {
            viewModelScope.launch {
                walksRepository.deleteWalk(walk)
                // No need to reload - Flow will automatically emit new value
            }
        }
    }
