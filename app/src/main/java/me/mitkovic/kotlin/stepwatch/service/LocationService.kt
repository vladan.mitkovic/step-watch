package me.mitkovic.kotlin.stepwatch.service

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.content.pm.ServiceInfo
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.os.Build
import android.os.Looper
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.Priority
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import me.mitkovic.kotlin.stepwatch.R
import me.mitkovic.kotlin.stepwatch.StepWatchApplication
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.data.repository.WalksRepository
import me.mitkovic.kotlin.stepwatch.logging.Logger
import me.mitkovic.kotlin.stepwatch.ui.MainActivity
import me.mitkovic.kotlin.stepwatch.utils.Constants
import me.mitkovic.kotlin.stepwatch.utils.DateTimeUtils
import me.mitkovic.kotlin.stepwatch.utils.DistanceUtils
import me.mitkovic.kotlin.stepwatch.utils.NumberUtils
import javax.inject.Inject

@AndroidEntryPoint
class LocationService :
    LifecycleService(),
    SensorEventListener {

    @Inject
    lateinit var walksRepository: WalksRepository

    @Inject
    lateinit var logger: Logger

    private val locationPoints = mutableListOf<LocationData>()

    private lateinit var locationRequest: LocationRequest
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var sensorManager: SensorManager

    private var walkId: Int = 0
    private var stepCount = 0
    private var startTime: Long = 0
    private var lastLocationUpdateTime: Long = 0
    private var reconnectAttempt = 0

    private var lastLocation: Location? = null

    private var reconnectJob: Job? = null

    companion object {
        const val ACTION_START = "ACTION_START"
        const val ACTION_STOP = "ACTION_STOP"
        const val LOCATION_SERVICE_CHANNEL = "LOCATION_SERVICE_CHANNEL"
        const val AVERAGE_STEP_LENGTH = 0.78 // average step length in meters
        const val ACCURACY_THRESHOLD = 100
        const val TIME_INTERVAL_MILLIS = 1000L
        const val MIN_UPDATE_INTERVAL_MILLIS = 500L
        const val MIN_UPDATE_DISTANCE_METERS = 1.0f
        const val MPS_TO_KPH = 3.6 // Conversion factor from meters per second to kilometers per hour
        const val SECONDS_IN_MINUTE = 60
        const val LOCATION_UPDATE_TIMEOUT = 5000L
    }

    override fun onCreate() {
        super.onCreate()
        logger.logDebug(LocationService::class.java.name, "Service created")
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        sensorManager = getSystemService(SENSOR_SERVICE) as SensorManager

        locationRequest =
            LocationRequest
                .Builder(Priority.PRIORITY_HIGH_ACCURACY, TIME_INTERVAL_MILLIS)
                .apply {
                    setMinUpdateIntervalMillis(MIN_UPDATE_INTERVAL_MILLIS)
                    setMinUpdateDistanceMeters(MIN_UPDATE_DISTANCE_METERS)
                }.build()

        val stepSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR)
        sensorManager.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_UI)
    }

    override fun onStartCommand(
        intent: Intent?,
        flags: Int,
        startId: Int,
    ): Int {
        super.onStartCommand(intent, flags, startId)

        when (intent?.action) {
            ACTION_START -> {
                logger.logDebug(LocationService::class.java.name, "Received ACTION_START")
                if (walkId == 0) { // Check if already started
                    startForegroundService()
                    startWalking()
                }
            }
            ACTION_STOP -> {
                logger.logDebug(LocationService::class.java.name, "Received ACTION_STOP")
                if (walkId != 0) { // Check if already stopped
                    stopWalking()
                    stopForeground(STOP_FOREGROUND_REMOVE)
                    stopSelf()
                }
            }
        }
        return START_NOT_STICKY
    }

    private fun startForegroundService() {
        logger.logDebug(LocationService::class.java.name, "Starting foreground service")
        val notification = createNotification()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            startForeground(1, notification, ServiceInfo.FOREGROUND_SERVICE_TYPE_LOCATION)
        } else {
            startForeground(1, notification)
        }
    }

    private fun startWalking() {
        logger.logDebug(LocationService::class.java.name, "Starting walking session")
        startTime = System.currentTimeMillis()
        createWalk()
        startLocationUpdates()
    }

    private fun createWalk() {
        val walk =
            Walk(
                name = "Walking",
                startTime = startTime,
                stopTime = 0,
                distance = 0.0,
                steps = 0,
                speed = 0.0,
            )
        lifecycleScope.launch {
            walkId = walksRepository.insertWalk(walk).toInt()
            logger.logDebug(LocationService::class.java.name, "Created walk with ID: $walkId")
        }
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        val locationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(locationRequest).build()
        val settingsClient = LocationServices.getSettingsClient(this)

        settingsClient
            .checkLocationSettings(locationSettingsRequest)
            .addOnSuccessListener {
                logger.logDebug(LocationService::class.java.name, "Location settings are satisfied")
                fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())
            }.addOnFailureListener { exception ->
                logger.logDebug(LocationService::class.java.name, "Location settings are not satisfied: $exception")
            }
    }

    private val locationCallback =
        object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                logger.logDebug(LocationService::class.java.name, "onLocationResult: $locationResult")
                lastLocationUpdateTime = System.currentTimeMillis() // Update last location update time

                for (location in locationResult.locations) {
                    logger.logDebug(
                        LocationService::class.java.name,
                        "Location accuracy: ${location.accuracy}, speed: ${location.speed}, provider: ${location.provider}",
                    )

                    val distanceToLast = lastLocation?.distanceTo(location) ?: 0f
                    logger.logDebug(LocationService::class.java.name, "Distance to last location: $distanceToLast")

                    if (location.accuracy <= ACCURACY_THRESHOLD) {
                        logger.logDebug(LocationService::class.java.name, "Location accuracy is within threshold")

                        val speedKmH = location.speed * MPS_TO_KPH

                        if (location.speed > 0.1) {
                            logger.logDebug(LocationService::class.java.name, "Significant movement detected")

                            val locationData =
                                LocationData(
                                    walkId = walkId,
                                    latitude = location.latitude,
                                    longitude = location.longitude,
                                    timestamp = System.currentTimeMillis(),
                                    speed = speedKmH,
                                    altitude = location.altitude,
                                )
                            locationPoints.add(locationData)

                            lifecycleScope.launch {
                                walksRepository.insertLocationData(locationData)
                                logger.logDebug(LocationService::class.java.name, "Inserted location data: $locationData")

                                val totalDistance = calculateTotalDistance(locationPoints)
                                // Update Walk's distance and steps
                                walksRepository.updateWalk(
                                    Walk(
                                        id = walkId,
                                        name = "Walking",
                                        startTime = startTime,
                                        stopTime = 0L,
                                        distance = totalDistance,
                                        steps = stepCount,
                                        speed = speedKmH,
                                    ),
                                )
                                logger.logDebug(
                                    LocationService::class.java.name,
                                    "Updated walk ID: $walkId with distance: $totalDistance M, steps: $stepCount",
                                )
                            }
                            lastLocation = location
                            logger.logDebug(LocationService::class.java.name, "Accurate location recorded: $location")

                            // Reset reconnection attempts on successful location update
                            resetReconnectAttempts()
                        } else {
                            logger.logDebug(LocationService::class.java.name, "Speed too low or no significant movement: $location")
                        }
                    } else {
                        logger.logDebug(LocationService::class.java.name, "Inaccurate location skipped: $location")
                    }
                }
            }
        }

    private fun calculateSpeed(): Double {
        val elapsedTime = (System.currentTimeMillis() - startTime) / TIME_INTERVAL_MILLIS // time in seconds
        val distance = stepCount * AVERAGE_STEP_LENGTH // assuming an average step length
        val speedMps = if (elapsedTime > 0 && distance > 1) distance / elapsedTime else 0.0 // speed in meters per second
        return speedMps * MPS_TO_KPH // convert to kilometers per hour
    }

    private suspend fun updateWalk() {
        val stopTime = System.currentTimeMillis()

        val name = "Walking - ${DateTimeUtils.formatDateTime(startTime)} / ${DateTimeUtils.formatDateTime(stopTime)}"
        val totalDistance = calculateTotalDistance(locationPoints) // Calculate total distance in meters

        val currentSpeed = calculateSpeed()
        logger.logDebug(LocationService::class.java.name, "currentSpeed: $currentSpeed")

        val currentSpeedNew =
            if (stopTime > startTime) {
                (totalDistance / ((stopTime - startTime) / 1000.0)) * MPS_TO_KPH
            } else {
                0.0
            }
        logger.logDebug(LocationService::class.java.name, "currentSpeedNew: $currentSpeedNew")

        try {
            val walkWithLocations = walksRepository.getWalkWithLocations(walkId).firstOrNull()
            walkWithLocations?.walk?.let { walk ->
                val updatedWalk =
                    walk.copy(
                        name = name,
                        stopTime = stopTime,
                        distance = totalDistance,
                        steps = stepCount,
                        speed = currentSpeed,
                    )
                walksRepository.updateWalk(updatedWalk)
                walkId = 0
                logger.logDebug(LocationService::class.java.name, "Walk updated: $updatedWalk")
            } ?: logger.logDebug(LocationService::class.java.name, "Walk with ID $walkId not found.")
        } catch (exception: Exception) {
            logger.logDebug(LocationService::class.java.name, "Exception during walk update: $exception")
        }
    }

    internal fun calculateTotalDistance(locations: List<LocationData>): Double {
        var totalDistance = 0.0
        for (i in 1 until locations.size) {
            val previous = locations[i - 1]
            val current = locations[i]
            totalDistance += DistanceUtils.haversine(previous.latitude, previous.longitude, current.latitude, current.longitude)
        }
        return totalDistance // Return distance in meters
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (walkId == 0) {
            // Do not update if walkId is zero (not walking)
            return
        }
        if (event?.sensor?.type == Sensor.TYPE_STEP_DETECTOR) {
            val currentTime = System.currentTimeMillis()
            if (currentTime - lastLocationUpdateTime > LOCATION_UPDATE_TIMEOUT && reconnectJob == null) {
                logger.logDebug(
                    LocationService::class.java.name,
                    "GPS signal lost. No location updates for more than ${LOCATION_UPDATE_TIMEOUT / 1000} seconds.",
                )
                reconnectToGps() // Attempt to reconnect to GPS
            } else {
                stepCount++
                val calculatedSpeed = calculateSpeed() // speed in km/h
                logger.logDebug(
                    LocationService::class.java.name,
                    "Step detected. Total steps: $stepCount, calculated speed: $calculatedSpeed km/h",
                )
            }
        }
    }

    override fun onAccuracyChanged(
        sensor: Sensor?,
        accuracy: Int,
    ) {
        // No action needed
        logger.logDebug(LocationService::class.java.name, "Sensor accuracy changed: $accuracy")
    }

    internal fun resetReconnectAttempts() {
        reconnectAttempt = 0
        reconnectJob?.cancel()
        reconnectJob = null
        logger.logDebug(LocationService::class.java.name, "Reconnection attempts reset")
    }

    private fun reconnectToGps() {
        // Convert minutes to milliseconds
        val delayTime = NumberUtils.fibonacci(reconnectAttempt).toLong() * SECONDS_IN_MINUTE * TIME_INTERVAL_MILLIS
        logger.logDebug(
            LocationService::class.java.name,
            "Attempting to reconnect GPS in ${NumberUtils.fibonacci(reconnectAttempt)} minutes.",
        )
        reconnectJob =
            lifecycleScope.launch {
                delay(delayTime)
                startLocationUpdates()
                reconnectAttempt++
            }
    }

    private fun stopWalking() {
        logger.logDebug(LocationService::class.java.name, "Stopping walking session")
        fusedLocationClient.removeLocationUpdates(locationCallback)
        sensorManager.unregisterListener(this)

        (application as StepWatchApplication).applicationScope.launch {
            updateWalk()
        }
    }

    private fun createNotification(): Notification {
        val notificationChannelId = LOCATION_SERVICE_CHANNEL
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(
                    notificationChannelId,
                    "Location Service",
                    NotificationManager.IMPORTANCE_HIGH,
                ).apply {
                    description = "Channel used by location service"
                }
            notificationManager.createNotificationChannel(channel)
        }

        // Create an intent to open your app
        val intent =
            Intent(this, MainActivity::class.java).apply {
                putExtra(Constants.FROM_NOTIFICATION, true) // Add an extra to indicate it was launched from the notification
            }
        val pendingIntent =
            PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE,
            )

        val notificationBuilder =
            NotificationCompat
                .Builder(this, notificationChannelId)
                .setContentTitle("Location Service")
                .setContentText("Collecting location, speed, and altitude data")
                .setSmallIcon(R.drawable.step_app_icon)
                .setContentIntent(pendingIntent)

        return notificationBuilder.build()
    }

    override fun onDestroy() {
        super.onDestroy()
        fusedLocationClient.removeLocationUpdates(locationCallback)
        sensorManager.unregisterListener(this)

        reconnectAttempt = 0
        reconnectJob?.cancel()
        reconnectJob = null

        logger.logDebug(LocationService::class.java.name, "Service destroyed")
    }
}
