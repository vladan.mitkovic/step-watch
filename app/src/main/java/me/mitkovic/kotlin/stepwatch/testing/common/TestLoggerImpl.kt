package me.mitkovic.kotlin.stepwatch.testing.common

import me.mitkovic.kotlin.stepwatch.logging.Logger

object TestLoggerImpl : Logger {

    var delegate: Logger? = null

    override fun logDebug(
        tag: String,
        message: String,
    ) {
        delegate?.logDebug(tag, message) ?: println("$tag: $message")
    }

    override fun logError(
        tag: String,
        message: String?,
        throwable: Throwable?,
    ) {
        delegate?.logError(tag, message, throwable) ?: println("$tag: $message")
    }
}
