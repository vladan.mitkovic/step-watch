package me.mitkovic.kotlin.stepwatch.data.repository

import kotlinx.coroutines.flow.Flow
import me.mitkovic.kotlin.stepwatch.data.local.WalkWithLocations
import me.mitkovic.kotlin.stepwatch.data.model.LocationData
import me.mitkovic.kotlin.stepwatch.data.model.Walk

interface WalksRepository {

    fun getActiveWalk(): Flow<Walk?>

    fun getWalks(): Flow<List<Walk>>

    fun getWalkWithLocations(walkId: Int): Flow<WalkWithLocations?>

    suspend fun insertWalk(walk: Walk): Long

    suspend fun updateWalk(walk: Walk)

    suspend fun deleteWalk(walk: Walk)

    suspend fun insertLocationData(locationData: LocationData)
}
