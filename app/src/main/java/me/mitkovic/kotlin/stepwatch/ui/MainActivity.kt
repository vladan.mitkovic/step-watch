package me.mitkovic.kotlin.stepwatch.ui

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.automirrored.filled.List
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.core.content.ContextCompat
import androidx.navigation.compose.rememberNavController
import dagger.hilt.android.AndroidEntryPoint
import me.mitkovic.kotlin.stepwatch.R
import me.mitkovic.kotlin.stepwatch.ui.commons.HandleErrorSnackbar
import me.mitkovic.kotlin.stepwatch.ui.navigation.Destination
import me.mitkovic.kotlin.stepwatch.ui.navigation.StepWatchAppNavigation
import me.mitkovic.kotlin.stepwatch.ui.theme.StepWatchTheme
import me.mitkovic.kotlin.stepwatch.utils.Constants
import timber.log.Timber

sealed class MainAction {
    data class TitleTextChanged(
        val title: String,
    ) : MainAction()

    data class ShowActionsChanged(
        val showActions: Boolean,
    ) : MainAction()

    data class ShowBackIconChanged(
        val showBackButton: Boolean,
    ) : MainAction()

    data class LaunchedFromNotificationChanged(
        val launchedFromNotification: Boolean,
    ) : MainAction()

    data class OnError(
        val error: String,
    ) : MainAction()
}

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val requestPermissionsLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions(),
        ) { permissions ->
            permissions.entries.forEach {
                Timber.tag(Constants.PERMISSIONS_TAG).d("Entry: ${it.key} = ${it.value}")
            }
        }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)

        requestPermissions()

        setContent {
            StepWatchTheme(dynamicColor = false) {
                val navController = rememberNavController()
                val topBarTitle = remember { mutableStateOf(getString(R.string.app_name)) }
                val showActions = remember { mutableStateOf(false) }
                val showBackIcon = remember { mutableStateOf(false) }

                // snackbarHostState - used for displaying eventual errors in Scaffold snack bar
                val snackbarHostState = remember { SnackbarHostState() }
                val errorMessageState = remember { mutableStateOf<Pair<String, Long>?>(null) }

                // back to walking screen by clicking on foreground service notification
                var launchedFromNotification by remember { mutableStateOf(false) }
                LaunchedEffect(Unit) {
                    launchedFromNotification =
                        intent?.extras?.getBoolean(Constants.FROM_NOTIFICATION, false) == true
                }

                Scaffold(
                    containerColor = MaterialTheme.colorScheme.background,
                    modifier = Modifier.fillMaxSize(),
                    topBar = {
                        TopAppBar(
                            title = {
                                Row(
                                    verticalAlignment = Alignment.CenterVertically,
                                ) {
                                    if (!showBackIcon.value) {
                                        Icon(
                                            painter = painterResource(id = R.drawable.step_app_icon),
                                            contentDescription = stringResource(R.string.content_description_application_icon),
                                            modifier = Modifier.size(dimensionResource(id = R.dimen.spacing_xlarge)),
                                            tint = MaterialTheme.colorScheme.onPrimary,
                                        )
                                        Spacer(modifier = Modifier.width(dimensionResource(id = R.dimen.spacing_small)))
                                    }
                                    Text(
                                        text = topBarTitle.value,
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis,
                                        style =
                                            MaterialTheme.typography.headlineLarge.copy(
                                                color = MaterialTheme.colorScheme.onPrimary,
                                            ),
                                    )
                                }
                            },
                            actions = {
                                if (showActions.value) {
                                    IconButton(onClick = {
                                        navController.navigate(Destination.Walks)
                                    }) {
                                        Icon(
                                            Icons.AutoMirrored.Filled.List,
                                            contentDescription = stringResource(R.string.content_description_walks_list),
                                            modifier = Modifier.size(dimensionResource(id = R.dimen.spacing_large_button)),
                                            tint = MaterialTheme.colorScheme.onPrimary,
                                        )
                                    }
                                }
                            },
                            navigationIcon = {
                                if (showBackIcon.value) {
                                    IconButton(onClick = { navController.popBackStack() }) {
                                        Icon(
                                            Icons.AutoMirrored.Filled.ArrowBack,
                                            modifier = Modifier.size(dimensionResource(id = R.dimen.icon_size)),
                                            contentDescription = stringResource(R.string.content_description_back_arrow),
                                            tint = MaterialTheme.colorScheme.onPrimary,
                                        )
                                    }
                                }
                            },
                            colors =
                                TopAppBarDefaults.centerAlignedTopAppBarColors(
                                    containerColor = MaterialTheme.colorScheme.background,
                                ),
                        )
                    },
                    snackbarHost = { SnackbarHost(snackbarHostState) },
                ) { paddingValues ->

                    // NavHost
                    StepWatchAppNavigation(
                        navHostController = navController,
                        paddingValues = paddingValues,
                        launchedFromNotification = { launchedFromNotification },
                        onAction = { action ->
                            when (action) {
                                is MainAction.TitleTextChanged -> {
                                    topBarTitle.value = action.title
                                }
                                is MainAction.ShowActionsChanged -> {
                                    showActions.value = action.showActions
                                }
                                is MainAction.ShowBackIconChanged -> {
                                    showBackIcon.value = action.showBackButton
                                }
                                is MainAction.LaunchedFromNotificationChanged -> {
                                    launchedFromNotification = action.launchedFromNotification
                                }
                                // Errors propagated from Walking screen thru NavHost up to here
                                is MainAction.OnError -> {
                                    errorMessageState.value = action.error to System.currentTimeMillis()
                                }
                            }
                        },
                    )
                }
                // Displaying errors in snack bar
                errorMessageState.value?.let { errorMessage ->
                    HandleErrorSnackbar(snackbarHostState = snackbarHostState, errorMessage = errorMessage)
                }
            }
        }
    }

    private fun requestPermissions() {
        val permissionsToRequest = mutableListOf<String>()
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            permissionsToRequest.add(Manifest.permission.ACCESS_FINE_LOCATION)
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            permissionsToRequest.add(Manifest.permission.ACCESS_COARSE_LOCATION)
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACTIVITY_RECOGNITION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                permissionsToRequest.add(Manifest.permission.ACTIVITY_RECOGNITION)
            }
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                permissionsToRequest.add(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
        if (permissionsToRequest.isNotEmpty()) {
            requestPermissionsLauncher.launch(permissionsToRequest.toTypedArray())
        }
    }
}
