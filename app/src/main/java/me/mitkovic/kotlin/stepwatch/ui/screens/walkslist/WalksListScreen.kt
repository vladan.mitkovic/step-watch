package me.mitkovic.kotlin.stepwatch.ui.screens.walkslist

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import me.mitkovic.kotlin.stepwatch.R
import me.mitkovic.kotlin.stepwatch.data.model.Walk
import me.mitkovic.kotlin.stepwatch.ui.commons.StyledIconButton
import me.mitkovic.kotlin.stepwatch.ui.commons.UserConfirmationDialog
import me.mitkovic.kotlin.stepwatch.utils.DateTimeUtils
import me.mitkovic.kotlin.stepwatch.utils.DistanceUtils

@Composable
fun WalksListScreen(
    viewModel: WalksListScreenViewModel = hiltViewModel(),
    paddingValues: PaddingValues,
    onWalkClick: (Int) -> Unit,
) {
    val walksUiState by viewModel.walksUiState.collectAsStateWithLifecycle()

    Column(modifier = Modifier.padding(paddingValues)) {
        if (walksUiState.isLoading) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center,
            ) {
                CircularProgressIndicator(color = MaterialTheme.colorScheme.onPrimary)
            }
        } else {
            LazyColumn(modifier = Modifier.fillMaxSize()) {
                val groupedWalks =
                    walksUiState.walks.walks
                        .sortedByDescending { it.startTime }
                        .groupBy { DateTimeUtils.formatDate(it.startTime) }

                groupedWalks.forEach { (date, walks) ->
                    item {
                        Box(
                            modifier =
                                Modifier
                                    .fillMaxWidth()
                                    .padding(dimensionResource(id = R.dimen.spacing_medium)),
                            contentAlignment = Alignment.CenterEnd,
                        ) {
                            Text(
                                text = date,
                                color = MaterialTheme.colorScheme.onSurface,
                                style = MaterialTheme.typography.titleMedium,
                            )
                        }
                    }
                    items(walks) { walk ->
                        WalkItem(
                            walk = walk,
                            onClick = { onWalkClick(walk.id) },
                            onDeleteClicked = { walkToDelete, confirmation ->
                                if (confirmation) {
                                    viewModel.removeWalk(walkToDelete)
                                }
                            },
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun WalkItem(
    walk: Walk,
    onClick: () -> Unit,
    onDeleteClicked: (Walk, Boolean) -> Unit,
) {
    val totalTime = DateTimeUtils.formatElapsedTime(walk.startTime, walk.stopTime)
    val distanceInKm = DistanceUtils.metersToKilometers(walk.distance)

    Box(
        modifier =
            Modifier
                .padding(dimensionResource(id = R.dimen.spacing_medium))
                .clip(RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)))
                .background(color = MaterialTheme.colorScheme.onBackground)
                .border(
                    width = 1.dp,
                    color = MaterialTheme.colorScheme.outline,
                    shape = RoundedCornerShape(dimensionResource(id = R.dimen.spacing_medium)),
                ),
    ) {
        Row(
            modifier =
                Modifier
                    .fillMaxWidth()
                    .clickable(onClick = onClick)
                    .padding(dimensionResource(id = R.dimen.spacing_medium)),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
        ) {
            Column(
                modifier = Modifier.weight(1f),
            ) {
                Text(
                    text = walk.name,
                    modifier = Modifier.padding(bottom = dimensionResource(id = R.dimen.spacing_small)),
                    color = MaterialTheme.colorScheme.onPrimary,
                    style = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.Bold), // Use bold font weight
                )
                Text(
                    text =
                        buildAnnotatedString {
                            withStyle(style = MaterialTheme.typography.titleMedium.toSpanStyle()) {
                                append(stringResource(id = R.string.total_time) + " ")
                            }
                            append(totalTime)
                        },
                    color = MaterialTheme.colorScheme.onSurface,
                )
                Text(
                    text =
                        buildAnnotatedString {
                            withStyle(style = MaterialTheme.typography.titleMedium.toSpanStyle()) {
                                append(stringResource(id = R.string.distance) + " ")
                            }
                            append("$distanceInKm km")
                        },
                    color = MaterialTheme.colorScheme.onSurface,
                )
            }

            // Delete button
            val showConfirmation = remember { mutableStateOf(false) }

            StyledIconButton(
                Icons.Default.Delete,
                contentDescription = stringResource(id = R.string.content_description_delete_walk),
                modifier = Modifier.padding(start = dimensionResource(id = R.dimen.spacing_medium)),
            ) {
                showConfirmation.value = true
            }

            if (showConfirmation.value) {
                UserConfirmationDialog(stringResource(id = R.string.delete_confirmation)) { confirmation ->
                    onDeleteClicked(walk, confirmation)
                    showConfirmation.value = false
                }
            }
        }
    }
}
