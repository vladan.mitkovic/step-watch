package me.mitkovic.kotlin.stepwatch.testing.ui.screens.walkslist

import me.mitkovic.kotlin.stepwatch.testing.data.repository.TestWalksRepositoryImpl
import me.mitkovic.kotlin.stepwatch.ui.screens.walkslist.WalksListScreenViewModel

object TestWalksListScreenViewModelProvider {

    fun provideWalksListScreenViewModel(): WalksListScreenViewModel =
        WalksListScreenViewModel(
            walksRepository = TestWalksRepositoryImpl,
        )
}
