package me.mitkovic.kotlin.stepwatch.utils

object Constants {

    const val FROM_NOTIFICATION = "FROM_NOTIFICATION"
    const val PERMISSIONS_TAG = "Permissions"
    const val SOMETHING_WENT_WRONG = "Something went wrong."
}
