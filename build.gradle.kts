// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    alias(libs.plugins.androidApplication) apply false
    alias(libs.plugins.jetbrainsKotlinAndroid) apply false
    alias(libs.plugins.compose.compiler) apply false
    alias(libs.plugins.hiltAndroid) apply false
    alias(libs.plugins.ksp) apply false
}

// to check which composables are stable/unstable
// usage: ./gradlew clean assembleRelease -PStepWatchApp=true -PStepWatchApp.enableComposeCompilerReports=true
// do this only for release version by using assembleRelease
// do Build -> Clean Project before executing command above. This will delete build folder with old files.
// after executing command above check app->build->compose_metrics->app_release_classes.txt file
subprojects {
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
        compilerOptions {
            val enableComposeCompilerReports = project.findProperty("StepWatchApp.enableComposeCompilerReports") as? String
            if (enableComposeCompilerReports == "true") {
                val buildDirPath =
                    project.layout.buildDirectory
                        .get()
                        .asFile.absolutePath
                freeCompilerArgs.addAll(
                    listOf(
                        "-P",
                        "plugin:androidx.compose.compiler.plugins.kotlin:reportsDestination=$buildDirPath/compose_metrics",
                        "-P",
                        "plugin:androidx.compose.compiler.plugins.kotlin:metricsDestination=$buildDirPath/compose_metrics",
                    ),
                )
            }
        }
    }
}
